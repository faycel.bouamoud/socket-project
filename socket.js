var socket_io = require('socket.io');
var io = socket_io();

var id = 0;
io.on('connection', (socket) => {
    console.log('a user connected !!');
    id ++;
    socket.emit('email', { from: "abc", to: 'zaer' });
    socket.on('createEmail', function (data) {
        if(data.to === 'everyone') {
            console.log(data.message);
            io.emit('createEmail', data.message);
        }
    });
});

module.exports.io = io;
module.exports.id = id;